function resizeHandler() {
	var mvp = document.getElementById('main-viewport'),
		content = 'width=device-width',
		scale = '1.0';

	if (screen.width > 480) {
		content = 'width=1140';
		scale = '1.0';
	}

	mvp.setAttribute('content', content);
	mvp.setAttribute('inital-scale', scale);
}

window.onload = resizeHandler;
window.onresize = resizeHandler;


$(window).on('resize orientationchange', function () {
	$('.slider-resize-fix').slick('resize');
});

$(document).ready(function () {

	var dates = [
		'11-01-2017',
		'11-02-2017',
		'11-03-2017',
		'11-06-2017',
		'11-07-2017',
		'11-10-2017',
		'11-11-2017',
		'11-10-2017',
		'11-25-2017',
		'11-28-2017',
		'11-29-2017',
		'11-30-2017'
	];

	function highlightDays(date) {
		for (var i = 0; i < dates.length; i++) {
			if (new Date(dates[i]).toString() == date.toString()) {
				return [true, 'highlight'];
			}
		}
		return [true, ''];
	}

	$('#datepicker01, #datepicker02, #datepicker03').datepicker({
		nextText: '<svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="img/icons.svg#icon-arrow-right"></use></svg>',
		prevText: '<svg xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="img/icons.svg#icon-arrow-left"></use></svg>',
		dateFormat: 'yy-mm-dd',
		beforeShowDay: highlightDays
	});

	$('.slided-content-control').slick({
		mobileFirst: true,
		arrows: false,
		asNavFor: '.slided-content',
		variableWidth: true,
		centerMode: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 768,
				settings: 'unslick'
			},
			// {
			// 	breakpoint: 767,
			// 	settings: {
			// 		slidesToShow: 1,
			// 		draggable: true,

			// 	}
			// },
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					draggable: true,
					centerMode: true,
					variableWidth: true,
					focusOnSelect: true,
				}
			},
			{
				breakpoint: 320,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					draggable: true,
					centerMode: true,
					variableWidth: true,
					focusOnSelect: true,
					// arrows: false
				}
			}
		]
	});

	$('.slided-content').slick({
		mobileFirst: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		asNavFor: '.slided-content-control',
		arrows: false,
		responsive: [
			{
				breakpoint: 1140,
				settings: {
					draggable: false,
					fade: true
				}
			},
			{
				breakpoint: 320,
				settings: {
					draggable: true,
					fade: false
				}
			}
		]
	});

	$(".slided-content-control li").click(function(e){
		$(this).toggleClass('active').siblings().removeClass('active');
		e.preventDefault();
		slideIndex = $(this).index();
		$('.slided-content').slick('goTo', parseInt(slideIndex));
	});


	$('.course-promo_backdrop-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		// draggable: true,
		arrows: false,
		fade: true,
	});

	$('.course-promo_content-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		draggable: true,
		arrows: false,
		asNavFor: '.course-promo_backdrop-slider'
	});

	$('.course-promo_slider-controls .slick-arrow_left').click(function () {
		$('.course-promo_content-slider').slick("slickPrev");
	});

	$('.course-promo_slider-controls .slick-arrow_right').click(function () {
		$('.course-promo_content-slider').slick("slickNext");
	});

	$('.course-slider').slick({
		mobileFirst: true,
		nextArrow: '<svg class="icon slick-arrow_right"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/icons.svg#icon-arrow-right"></use></svg>',
		prevArrow: '<svg class="icon slick-arrow_left"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/icons.svg#icon-arrow-left"></use></svg>',
		responsive: [
			{
				breakpoint: 768,
				settings: 'unslick'
			},
			// {
			// 	breakpoint: 767,
			// 	settings: {
			// 		slidesToShow: 2,
			// 		draggable: true,
			// 		arrows: true,
			// 	}
			// },
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					// draggable: true,
					// arrows: true,
				}
			}
		]
	});










	$('.slided-small-gallery').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		draggable: false,
		arrows: false,
		fade: true,
		asNavFor: '.slided-small-gallery-controls',
	});

	$('.slided-small-gallery-controls').slick({
		mobileFirst: true,
		asNavFor: '.slided-small-gallery',
		slidesToShow: 4,
		slidesToScroll: 1,
		draggable: false,
		arrows: false,
		centerMode: true,
		variableWidth: true,
		focusOnSelect: true,
		responsive: [
			{
				breakpoint: 320,
				settings: {
					draggable: false
				}
			}
		]
	});













	$('.fancybox_image').fancybox({
		type: 'image',
		openEffect: 'fade',
		closeEffect: 'fade',
		mouseWheel: false,
		scrolling: 'no',
		openSpeed: 200,
		closeSpeed: 200,
		nextMethod: 'zoomIn',
		nextSpeed: 200,
		helpers: {
			overlay: {
				locked: true,
				speedIn: 200,
				speedOut: 200,
				showEarly: true
			}
		},
		tpl: {
			closeBtn: '<a title="Закрыть" class="fancybox-item fancybox-close" href="javascript:;"></a>',
			next: '<a title="Следующий" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
			prev: '<a title="Предыдущий" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>',
			error: '<p class="fancybox-error">Запрошенное изображение не может быть загружено.<br/>Повторите попытку позже.</p>'
		}
	});




});
